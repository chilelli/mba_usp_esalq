sRootPath = 'C:\\Users\\Chilelli\\Documents\\MBA\\TCC\\Olist\\'

# Root files
sCustomersFile = sRootPath + 'original\\olist_customers_dataset.csv'
sOrderItemsFile = sRootPath + 'original\\olist_order_items_dataset.csv'
sOrderPaymentsFile = sRootPath + 'original\\olist_order_payments_dataset.csv'
sOrderReviewsFile = sRootPath + 'original\\olist_order_reviews_dataset.csv'
sOrdersFile = sRootPath + 'original\\olist_orders_dataset.csv'
sProductsFile = sRootPath + 'original\\olist_products_dataset.csv'
sSellersFile = sRootPath + 'original\\olist_sellers_dataset.csv'
sProdCategoryFile = sRootPath + 'original\\product_category_name_translation.csv'

# Cleaned Files
sOrdersCleaned =  sRootPath + 'order_complete.csv'
sOrdersItemsCleaned =  sRootPath + 'order_items.csv'
sOrdersPayCleaned =  sRootPath + 'oder_payments.csv'

# Query Baseline
sBaseline = sRootPath + 'query_baseline.csv'
sTargetComplete = sRootPath + 'target_train.csv'
sValidation = sRootPath + 'target_validation.csv'

sAttComplete = sRootPath + 'att_complete.csv'
sAttValidation = sRootPath + 'att_validation.csv'

# Meses de Target

ssCutDates = [
    '2017-07-01',
    '2017-08-01',
    '2017-09-01',
    '2017-10-01',
    '2017-11-01',
    '2017-12-01',
    '2018-01-01',
    '2018-02-01',
    '2018-03-01',
    '2018-04-01',
    '2018-05-01',
    '2018-06-01',
    '2018-07-01',
    '2018-08-01'
]

ssTarget = [
    '2017-7',
    '2017-8',
    '2017-9',
    '2017-10',
    '2017-11',
    '2017-12',
    '2018-1',
    '2018-2',
    '2018-3',
    '2018-4',
    '2018-5',
    '2018-6',
    '2018-7'
]

ssValidationDate = ['2018-8']


iRandomState = 42